package nz.co.apitalent;

import junit.framework.Assert;
import org.junit.Test;

public class TestApiTalentApp {

    @Test
    public void testGetVersion() {
        Assert.assertTrue(ApiTalentApp.getVersion().startsWith("1."));
    }

}
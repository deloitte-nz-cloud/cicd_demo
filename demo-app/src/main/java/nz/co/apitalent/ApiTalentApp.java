package nz.co.apitalent;

public class ApiTalentApp {

    public static void main(String[] args) {
        System.out.println("API Talent App Version = " + getVersion());
    }

    public static String getVersion() {
        return "1.0";
    }
}
<img style = 'float: right; width: 75px' src=./apitalent_logo.png>

# FAQ

## Is this CICD Demo serverless?

Yes. The CICD pipeline of this demo is serverless as it relies on AWS managed service such as AWS CloudFormation, AWS CodeCommit, AWS CodePipeline, AWS CodeBuild, AWS Lambda, Amazon S3 and AWS ECS. However it deploys a non serverless app.

## I require to run a similar pipeline in another AWS region. How can I achieve this requirement?

AWS CodePipeline is fully integrated with IAM and AWS CloudFormation so with appropriate permissions you can run the same CloudFormation template to spin up a similar pipeline in another region (subject to availability of the services of course).


## I have a Jenkins fleet, can I integrate it to this pipeline?

Yes you can replace AWS CodeBuild with Jenkins for executing build, test or deploy steps. AWS CodePipeline can delegate the build stage(s) to Jenkins meaning you can leverage existing Jenkins installations and configurations.


## My code is hosted on GitHub, should I move it to CodeCommit?

No. You can use AWS CodeCommit, Amazon S3 and GitHub as Source provider for AWS CodePipeline. Support for Bitbucket is coming soon as well. However if your code is hosted on an unsupported SCM such as SVN, you might need to migrate it to a supported SCM first or a custom code to push your changes from your existing repository to a supported Source provider.


## I am concerned about security and I do not want to release every builds to production. How can I achieve an end to end security tracability for every build?

IAM is the main key service of AWS and it is easy to setup users, groups, roles and policies to restrict what a user or a service can do. You can implement manual approvals for pushing a new release to production and restrict the approval role to be assumed by a small group of specified known users. By using roles you ensure that each service and user interacting with the pipeline have appropriate permissions and AWS CloudTrail helps you by keep tracking all the API calls on your account.


## My project does not require to be built but simply tested, can I still use CodePipeline?

Yes it is a perfect valid use case for AWS CodePipeline. With AWS CodePipeline, the Source stage is mandatory. You can also set up multiples Source, Build, Invoke, and Deploy stages to create more complex pipelines.


## My project requires complex dependencies management. How can I use AWS CodeBuild in that case?

If you require to manage large, private, 3rd party depencies or you are subject to licensing constraints, you can still leverage AWS CodeBuild. Because it is based on docker containers, an easy solution is to build your own Docker container embedding your dependencies. You can then push it to AWS ECS and  reference it in your AWS CodeBuild project as a build image. Your project can now takes advantage of the container's content easily without the need for you to manage any servers.


## My project generates test and coverage reports, how can I make them available?

Because AWS CodeBuild is stateless, the reports must be exported to a durable storage such as Amazon S3 at the end of each build. You can then expose these reports directly from Amazon S3 (website hosting) or run a Lambda function to export them to a Jenkins server or a web server such as Apache Tomcat.


## We follow a Git branching model and we require to build every branch and pull requests. How can I do that with AWS CodePipeline?

Currently, AWS CodePipeline can build only one branch at the time so some CI/CD tools such as Jenkins or Travis CI might be more appropriate for this scenario. However by leveraging AWS services such as AWS Lambda, it is possible to automate the creation of a pipeline for each branch. Because each AWS CodePipeline is free during its first 30 days, it is a perfect use case especially if the branches live shortly.

![Alt text](./MultiBranchesPipeline.png "Creation of pipelines on demand")


## How can I access the logs of a AWS CodePipeline?

On AWS, the logs are exported to AWS CloudWatch Logs so you can access them via this service. By default the logs do not expire so you can still review them once the pipeline has been deleted. You can also export them to an external 3rd party such as Splunk or SumoLogic.


## What is the cost of this CICD pipeline?

The pipeline is serverless and running on AWS so its pricing is described as pay-as-you-go.

Cost for this demo in Sydney region - without the free tier:

- CodeBuild: $0.05 (considering a 10 minutes build scenario on build.general1.small)
- CodePipeline: $1
- CodeCommit: $1
- CloudFormation: free
- Lambda: $0.20
- S3: $0.045 (storage + requests + data retrievals)
=> Max total cost: __$2.70__

Notes:

- that does not include the price of the deployed application itself.
- Under the free tier, this pipeline is free.

### Pricing details

AWS CodeCommit offers 5 active users per month per account for free then $1 per additional active user per month. Each free user gets unlimited repositories, 50 GB-month of storage and 10,000 Git requests/months. More details about AWS CodeCommit pricing is available [here](https://aws.amazon.com/codecommit/pricing/).

AWS CodeBuild starts with a cost of half a cent per minute (a two minute build cost $0.01) for a build.general1.small (3GB or RAM and 2 vCPU). The AWS CodeBuild infinite free tier includes 100 build minutes of build.general1.small per month. More details about AWS CodeBuild pricing is available [here](https://aws.amazon.com/codebuild/pricing/).

A new AWS CodePipeline pipeline is free for the first 30 days (to encourage experimentation) then costs $1 per active pipeline (active pipeline = at least one execution in a month). The AWS CodePipeline includes one free active pipeline each month. Because AWS CodePipeline relies on Amazon S3 for artifact storage some additional charges may apply. More details about AWS CodePipeline pricing is available [here](https://aws.amazon.com/codepipeline/pricing/).

Amazon S3 free tier include 5 GB of storage, 20,000 Get requests, 2,000 Put requests, and 15 GB of data transfer our each month for one year. Storage and request pricings vary per region but it is less than $0.03 per stored GB (except Sao Paulo region). Requests are also cheap ($0.01 for 1,000 Put request for example in Sydney region). More details about Amazon S3 pricing is available [here](https://aws.amazon.com/s3/pricing/).

AWS Lambda free tier includes 1M free requests per month and 400,000 GB-seconds of compute time per month. AWS Lambda then charges $0.20 per 1 million request thereafter. More details about AWS Lambda pricing is available [here](https://aws.amazon.com/lambda/pricing/).

AWS CloudFormation is free but the resources created so refer to appropriate pricing pages if required.
echo 'Loading envars from file '$1
ENVARS=''
while IFS= read -r line
do
  ENVARS=$ENVARS' --env '$line
done < $1
DOCKER_INSTANCE=$(docker run -it -d --rm $ENVARS apitalent:latest)
sleep 2
docker exec -it $DOCKER_INSTANCE /bin/bash

#!/usr/bin/env python

from distutils.dir_util import copy_tree
import getopt
import logging
from os import chdir, getcwd, walk, remove
from os.path import exists, join
import re
import shutil
import subprocess
import sys
import tempfile
import time
import yaml

import argparse
import boto3
from boto3.s3.transfer import TransferConfig, S3Transfer
from botocore.exceptions import ClientError


SUCCESS = 'success'
FAILED = 'failed'
IN_PROGRESS = 'in-progress'

CURRENT_PATH = getcwd()
DEPLOY_INFO_TEMP_FOLDER_KEY = 'temp_folder'
DEPLOY_INFO_FILE = join(CURRENT_PATH, 'deploy-info.yml')
TEMP_FOLDER_PREFIX = '__demo-cicd'


def is_stack_existing(client_cfn, stack_name):
    """
    Is a stack with specific name already exist?

    Args:
        client_cfn: the boto3 client to use for interacting with CloudFormation.
        stack_name: the name of the stack to check.

    Returns: True if the stack else False.

    Raises: any exception raised by .describe_stacks other than
    """
    try:
        client_cfn.describe_stacks(StackName=stack_name)
        return True
    except ClientError as error:
        if error.response['Error']['Code'] == 'ValidationError':
            logging.debug(str(error))
            return False
        else:
            raise  # unexpected error so raise it as it


def prepare_session(profile):
    """ Prepares session """
    try:
        logging.debug('Starting: prepare_session()')
        if profile is None:
            return boto3.Session()
        else:
            return boto3.Session(profile_name=profile)
    except:
        logging.critical('Failed to prepare AWS session! Check credentials file!')
    finally:
        logging.debug('Finishing: prepare_session()')


def parse_configuration(config_file):
    """
    Parses configuration.

    Args:
        config_file: the full of the config to load.

    Returns: the config loaded as a dict.
    """
    try:
        logging.debug('Starting: parse_configuration()')
        with open(config_file) as f:
            config = yaml.load(f)

        logging.debug('Configuration:')
        logging.debug('--------------')
        logging.debug(str(config))
        return config
    except:
        logging.critical('Failed to parse configuration file %s!', config_file)
        raise
    finally:
        logging.debug('Finishing: parse_configuration()')


def parse_arguments():
    """ Parses arguments """
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-a', '--action', choices=['cleanup-demo', 'prepare-demo', 'run-demo'],
        help='Action to exectute: cleanup-demo, prepare-demo or run-demo'
    )
    parser.add_argument(
        '-c', '--configfile', default='deploy-pipeline-config.yml',
        help='YAML-formatted configuration file'
    )
    parser.add_argument(
        '-p', '--profile',
        help='Name of AWS profile to use for execution. ' \
        'The profile must already exist in ~/.aws/config. ' \
        'This script blocks if the profile requires MFA sign-in.'
    )
    parser.add_argument(
        '-v', '--verbosity', action='count', default=0,
        help='Increase output verbosity'
    )
    parser.add_argument(
        '-f', '--force', action='store_true',
        help='Force non-interactive mode. The user will not be prompted to confirm any actions.'
    )

    return parser.parse_args()


def configure_logging(verbosity):
    """ Configures logging """
    level = logging.INFO
    if verbosity >= 1:
        level = logging.DEBUG

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=level)


def configure_transfer(s3):
    """ Configures S3 transfer """
    try:
        logging.debug('Starting: configure_transfer()')
        transfer_config = TransferConfig(
            multipart_threshold=(512 * 1024 * 1024),
            max_concurrency=10
        )
        return S3Transfer(s3)
    except Exception as e:
        logging.critical('Failed to create S3 transfer object!')
        logging.critical('Error Message: %s', e)
    finally:
        logging.debug('Finishing: configure_transfer()')


def upload_file(client_s3, source_file, strip_string, bucket_name):
    """
    Uploads a single file to s3.

    Args:
        client_s3: the boto3 client to use for interacting with S3.
        source_dir: <>
        strip_string: <>
        bucket_name: the name of the S3 bucket to upload files to.

    Raises:
        Exception: if anything wrong happens during the upload to S3.
    """
    try:
        # Compose the target filename, including path
        target_filename = source_file.split(strip_string, 1)[-1]
        transfer = configure_transfer(client_s3)
        if transfer:
            logging.info('Transferring %s --> %s/%s', source_file, bucket_name, target_filename)
            transfer.upload_file(source_file, bucket_name, target_filename)
    except Exception as e:
        logging.critical('Transfer failed! %s --> %s/%s', source_file, bucket_name, target_filename)
        logging.critical('Error Message: %s', e)
        raise


def upload_files(client_s3, source_dir, strip_string, bucket_name):
    """
    Uploads all the files from a source directory to a S3 bucket.

    Args:
        client_s3: the boto3 client to use for interacting with S3.
        source_dir: <>
        strip_string: <>
        bucket_name: the name of the S3 bucket to upload files to.
    """
    for subdir, dirs, files in walk(source_dir):
        for file in files:
            source_file = join(subdir, file)
            upload_file(client_s3, source_file, strip_string, bucket_name)


def delete_cfn_stack(client_cfn, stack_name):
    """
    Delete a CFn stack. If the stack does not exist, no action is attempted.

    Args:
        client_cfn: the boto3 client to use for interacting with CloudFormation.
        stack_name: the name of the stack to delete
    """
    if not is_stack_existing(client_cfn, stack_name):
        logging.info('CFn stack %s not found so not delete attempted...', stack_name)
        return

    logging.info('Deleting CFn stack %s...', stack_name)

    client_cfn.delete_stack(StackName=stack_name)
    waiter_cfn = client_cfn.get_waiter('stack_delete_complete')
    waiter_cfn.wait(StackName=stack_name)

    logging.info('CFn stack %s deleted', stack_name)


def manage_cfn_stack(client_cfn, stack_config):
    """
    Manage (create or update) a CFn stack.

    Args:
        client_cfn: the boto3 client to use for interacting with CloudFormation.
        stack_config: the parametrs of the stack.
    """
    region = client_cfn.meta.region_name
    stack_name = stack_config['name']

    # Determine action to make on this stack
    action = 'create' if not is_stack_existing(client_cfn, stack_name) else 'update'

    # Manage template's parameters
    template_params = []
    try:
        for key, value in stack_config['params'].iteritems():
            try:
                # Potential first group is $OUTPUT, second is the name of the stack and third
                # is the name of the output we want to use a value.
                # If no groups, an AttributeError is raised so that means no special treatment for
                # this particular value
                groups = re.search('(\$OUTPUT):(.*?):(.*?)$', value).groups()
                outputs = client_cfn.describe_stacks(StackName=groups[1])['Stacks'][0]['Outputs']
                for output in outputs:
                    # Find the correct output and set value
                    # then exit immediately (not possible to have to two outputs with same key)
                    if output['OutputKey'] == groups[2]:
                        value = output['OutputValue']
                        break
            except AttributeError as e:
                pass

            param = {
                'ParameterKey': key,
                'ParameterValue': value,
                'UsePreviousValue': False
            }
            template_params.append(param)
    except Exception as error:
        logging.critical(
            'Error during configuration of template parameters for stack %s', stack_name
        )
        raise error

    # Create params dict for the create_stack/update_stack call:
    params = {
        'StackName': stack_name,
        'Parameters': template_params,
        'Capabilities': ['CAPABILITY_NAMED_IAM']
    }
    template_loc_type = stack_config['template']['loc_type'].lower()
    template_location = stack_config['template']['location']

    if template_loc_type == 'local':
        filename = template_location + '/' + stack_config['template']['path'].strip('/')
        with open(filename) as template_file:
            template_body = template_file.read()
        # Update params dict with TemplateBody value
        params.update({'TemplateBody': template_body})
    elif template_loc_type == 's3':
        template_url = ''.join([
            'https://s3-{}.amazonaws.com/'.format(region),
            template_location,
            '/',
            stack_config['template']['path']
        ])
        # Update params dict with TemplateURL value
        params.update({'TemplateURL': template_url})
    else:
        raise Exception('Unknown template location type: {} (expected: s3 or local)'.format(
            template_loc_type
        ))

    try:
        if action == 'create':
            response = client_cfn.create_stack(**params)
        elif action == 'update':
            params.update({'UsePreviousTemplate': False})
            response = client_cfn.update_stack(**params)
    except Exception as error:
        if action == 'update':
            if error.response['Error']['Message'] == 'No updates are to be performed.':
                logging.info('Stack %s already up-to-date!', stack_name)
                return
        else:
            raise error

    # Wait for CFn stack to be created/updated successfully
    logging.debug('Waiting for stack %s to be ready', stack_name)
    waiting_status = 'stack_create_complete' if action == 'create' else 'stack_update_complete'
    waiter = client_cfn.get_waiter(waiting_status)
    waiter.wait(
        StackName=stack_name,
        # Wait time is 20 minutes max (120 x 10 seconds)
        WaiterConfig={'Delay': 10, 'MaxAttempts': 120}
    )


def main():
    """ Main method """
    args = parse_arguments()

    configure_logging(args.verbosity)

    config = parse_configuration(args.configfile)
    session = prepare_session(args.profile)

    # Create session/boto3 clients in the appropriate deployment region
    client_cfn = session.client('cloudformation', region_name=config['region'])
    client_s3 = boto3.client('s3', region_name=config['region'])

    if args.action == 'cleanup-demo':
        # Clean up the demo-app stack
        stack_name = config['stacks']['pipeline']['params']['AppServiceStackName']
        delete_cfn_stack(client_cfn, stack_name)

        # Clean up the pipeline stack
        stack_name = config['stacks']['pipeline']['name']
        if is_stack_existing(client_cfn, stack_name):
            try:   # clean up the ECR registry first...
                ecr_name = config['stacks']['pipeline']['params']['ECRRepoName']
                logging.info('Deleting ECR %s...', ecr_name)
                client_ecr = boto3.client('ecr', region_name=config['region'])
                client_ecr.delete_repository(
                    repositoryName=config['stacks']['pipeline']['params']['ECRRepoName'],
                    force=True  # to force the registry to be deleted even images are present
                )
                logging.info('ECR %s deleted', ecr_name)
            except Exception as error:
                print error

            delete_cfn_stack(client_cfn, stack_name)

        # Clean up the environment stack
        stack_name = config['stacks']['environment']['name']
        delete_cfn_stack(client_cfn, stack_name)

        # Clean up the prerequisites stack
        stack_name = config['stacks']['prerequisites']['name']
        if is_stack_existing(client_cfn, stack_name):
            # Empty the buckets first
            resource_s3 = boto3.resource('s3', region_name=config['region'])
            # Note: assumption made that every param is the name of a bucket...
            for param_name, param_value in config['stacks']['prerequisites']['params'].iteritems():
                try:
                    logging.info('Deleting bucket %s', param_value)
                    resource_s3.Bucket(param_value).object_versions.delete()
                    logging.info('Bucket %s deleted', param_value)
                except Exception as error:
                    print error

            delete_cfn_stack(client_cfn, stack_name)

        # Clean up CloudWatch logs
        client_logs = boto3.client('logs', region_name=config['region'])
        prefixes = [
            '/aws/codebuild/{}'.format(config['stacks']['pipeline']['name']),
            '/aws/codebuild/PackageCloudFormationTemplates',
            '/aws/lambda/{}'.format(config['stacks']['pipeline']['name']),
            '{}'.format(config['stacks']['pipeline']['params']['AppServiceStackName'])
        ]
        for prefix in prefixes:
            try:
                log_groups = client_logs.describe_log_groups(logGroupNamePrefix=prefix)['logGroups']

                for log_group in log_groups:
                    logging.info('Deleting Log Group %s...)', log_group['logGroupName'])
                    client_logs.delete_log_group(logGroupName=log_group['logGroupName'])
                    logging.info('Log Group %s deleted', log_group['logGroupName'])
            except Exception as error:
                print error

        # Clean up local temp folder and file
        subprocess.call('rm -rf {}*'.format(TEMP_FOLDER_PREFIX), shell=True)
        try:
            remove(DEPLOY_INFO_FILE)
        except:
            pass

        logging.info('Clean up is now complete...')
        return

    if args.action == 'prepare-demo':
        # Create the prerequisites stack and upload files to the S3 buckets
        manage_cfn_stack(client_cfn, config['stacks']['prerequisites'])
        upload_files(
            client_s3,
            './docker-image-source-repo/cfn',
            './docker-image-source-repo/',
            config['cfn_upload_bucket'],
        )

        upload_files(
            client_s3,
            './lambda',
            './',
            config['cfn_upload_bucket']
        )

        # Create/update the environment stack
        manage_cfn_stack(client_cfn, config['stacks']['environment'])

    if args.action == 'run-demo':
        # Create or update the pipeline itself
        manage_cfn_stack(client_cfn, config['stacks']['pipeline'])

        # Check that the initial deployment has been made or not...
        if exists(DEPLOY_INFO_FILE):
            with open(DEPLOY_INFO_FILE) as deploy_file:
                info = yaml.load(deploy_file)
            logging.info(
                'Initial deployment has been made, '\
                'CodeCommit repositories have been checked out in %s...'
                , info[DEPLOY_INFO_TEMP_FOLDER_KEY]
            )
            return

        # Not initial deployment made so we need to populate CodeCommit repositories only once...
        # Note that:
        # ==> YOUR IAM USER MUST BE AN ADMIN!
        # ==> YOUR IAM USER MUST ALREADY HAVE GIT CREDENTIALS!
        temp_dir = tempfile.mkdtemp(dir=CURRENT_PATH, prefix=TEMP_FOLDER_PREFIX)
        chdir(temp_dir)  # move to temp dir
        logging.info('Created temporary folder location => %s', temp_dir)

        try:
            # Configuration of git to use CodeCommit
            subprocess.call(
                "git config --global credential.helper '!aws codecommit credential-helper $@'; " \
                'git config --global credential.UseHttpPath true',
                shell=True
            )

            demo_app_repo_name = config['stacks']['pipeline']['params']['DemoAppSourceRepoName']
            docker_repo_name = config['stacks']['pipeline']['params']['DockerImageSourceRepoName']
            clone_url = 'git clone https://git-codecommit.{}.amazonaws.com/v1/repos/{}'
            init_commit = 'git add -A; git commit -m "Initial Commit..."; git push'

            # Checkout CodeCommit repositories
            subprocess.call(clone_url.format(config['region'], demo_app_repo_name), shell=True)
            subprocess.call(clone_url.format(config['region'], docker_repo_name), shell=True)

            # Copy local code to CodeCommit repositories
            copy_tree(join(CURRENT_PATH, 'demo-app'), join('.', demo_app_repo_name))
            copy_tree(join(CURRENT_PATH, 'docker-image-source-repo'), join('.', docker_repo_name))

            # Push code to CodeCommit
            chdir(join(temp_dir, demo_app_repo_name))
            subprocess.call(init_commit, shell=True)
            chdir(join(temp_dir, docker_repo_name))
            subprocess.call(init_commit, shell=True)

            # Save a local empty file to indicate that original commits/deployment have been made...
            with open(DEPLOY_INFO_FILE, 'w') as outfile:
                yaml.dump({DEPLOY_INFO_TEMP_FOLDER_KEY: temp_dir}, outfile)
        except Exception as e:
            logging.error('Exception => %s', str(e))


if __name__ == "__main__":
   main()
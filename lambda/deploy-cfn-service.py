from __future__ import print_function

import boto3
from boto3.session import Session
import botocore
import json
import tempfile
import traceback
import urllib
import zipfile


print('Loading function')

code_pipeline = boto3.client('codepipeline')


def find_artifact(artifacts, name):
    """
    Finds the artifact 'name' among the 'artifacts'.

    Args:
        artifacts: The list of artifacts available to the function
        name: The artifact we wish to use

    Returns:
        The artifact dictionary found

    Raises:
        Exception: If no matching artifact is found
    """
    for artifact in artifacts:
        if artifact['name'] == name:
            return artifact

    raise Exception('Input artifact named "{0}" not found in event'.format(name))


def get_template(client_s3, artifact, file_in_zip):
    """
    Downloads the artifact from the S3 artifact store to a temporary file
    then extracts the zip and returns the file containing the CloudFormation
    template.

    Args:
        client_s3: the boto3 client to use for interacting with S3.
        artifact: the artifact to download
        file_in_zip: the path to the file within the zip containing the template

    Returns:
        The CloudFormation template as a string

    Raises:
        Exception: any exception thrown while downloading the artifact or unzipping it
    """
    tmp_file = tempfile.NamedTemporaryFile()
    bucket = artifact['location']['s3Location']['bucketName']
    key = artifact['location']['s3Location']['objectKey']

    with tempfile.NamedTemporaryFile() as tmp_file:
        client_s3.download_file(bucket, key, tmp_file.name)
        with zipfile.ZipFile(tmp_file.name, 'r') as zip:
            return zip.read(file_in_zip)


def update_stack(stack, template, service_params, client_cfn):
    """
    Starts a CloudFormation stack update.

    Args:
        stack: the stack to update
        template: the template to apply
        service_params: the parameters required to be passed to the service stack when creating/updating
        client_cfn: the boto3 client to use for interacting with CloudFormation.

    Returns:
        True if an update was started, false if there were no changes
        to the template since the last update.

    Raises:
        Exception: any exception besides "No updates are to be performed."
    """
    try:
        client_cfn.update_stack(
            StackName=stack,
            TemplateBody=template,
            Parameters=service_params,
            Capabilities=['CAPABILITY_NAMED_IAM']
        )
        return True
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Message'] == 'No updates are to be performed.':
            return False
        else:
            raise Exception('Error updating CloudFormation stack "{0}"'.format(stack), e)


def stack_exists(stack, client_cfn):
    """
    Checks the existence of a CloudFormation stack.

    Args:
        stack: the stack to check
        client_cfn: the boto3 client to use for interacting with CloudFormation.

    Returns:
        True or False depending on whether the stack exists.

    Raises:
        Exception: any exceptions raised .describe_stacks() besides that the stack doesn't exist.
    """
    try:
        client_cfn.describe_stacks(StackName=stack)
        return True
    except botocore.exceptions.ClientError as e:
        if 'does not exist' in e.response['Error']['Message']:
            return False
        else:
            raise e


def create_stack(stack, template, service_params, client_cfn):
    """
    Initiates the creation of a new CloudFormation stack.

    Args:
        stack: the stack to be created.
        template: the template for the stack to be created with.
        service_params: the parameters required to be passed to the service
                        stack when creating/updating.
        client_cfn: the boto3 client to use for interacting with CloudFormation.

    Throws:
        Exception: any exception thrown by .create_stack()
    """
    client_cfn.create_stack(
        StackName=stack,
        TemplateBody=template,
        Parameters=service_params,
        Capabilities=['CAPABILITY_NAMED_IAM']
    )


def get_stack_status(stack, client_cfn):
    """
    Gets the status of an existing CloudFormation stack.

    Args:
        stack: the name of the stack to get status.
        client_cfn: the boto3 client to use for interacting with CloudFormation.

    Returns:
        The CloudFormation status string of the stack such as CREATE_COMPLETE

    Raises:
        Exception: any exception thrown by .describe_stacks()
    """
    return client_cfn.describe_stacks(StackName=stack)['Stacks'][0]['StackStatus']


def put_job_success(job, message):
    """
    Notifies CodePipeline of a successful job.

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_success_result()
    """
    print('Putting job success')
    print(message)
    code_pipeline.put_job_success_result(jobId=job)


def put_job_failure(job, message):
    """
    Notifies CodePipeline of a failed job

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_failure_result()
    """
    print('Putting job failure')
    print(message)
    code_pipeline.put_job_failure_result(
        jobId=job,
        failureDetails={'message': message, 'type': 'JobFailed'}
    )


def continue_job_later(job, message):
    """
    Notifies CodePipeline of a continuing job.

    This will cause CodePipeline to invoke the function again with the supplied continuation token.

    Args:
        job: the JobID.
        message: a message to be logged relating to the job status.
        continuation_token: the continuation token.

    Raises:
        Exception: any exception thrown by .put_job_success_result()
    """
    # Use the continuation token to keep track of any job execution state
    # This data will be available when a new job is scheduled to continue the current execution
    continuation_token = json.dumps({'previous_job_id': job})

    print('Putting job continuation')
    print(message)
    code_pipeline.put_job_success_result(jobId=job, continuationToken=continuation_token)


def start_update_or_create(job_id, stack, template, service_params, client_cfn):
    """
    Starts the stack update or create process. If the stack exists then update, otherwise create.

    Args:
        job_id: the CodePipeline job ID.
        stack: the stack to create or update.
        template: the template to create/update the stack with.
        service_params: the parameters required to be passed to the service stack
                        when creating/updating.
        client_cfn: the boto3 client to use for interacting with CloudFormation.
    """
    if stack_exists(stack, client_cfn):
        status = get_stack_status(stack, client_cfn)
        valid_statuses = [
            'CREATE_COMPLETE', 'ROLLBACK_COMPLETE', 'UPDATE_COMPLETE', 'UPDATE_ROLLBACK_COMPLETE'
        ]
        if status not in valid_statuses:
            # If the CFn stack is not in a state where it can be updated again
            # then fail the job right away.
            put_job_failure(job_id, 'Stack cannot be updated when status is: ' + status)
            return

        were_updates = update_stack(stack, template, service_params, client_cfn)

        if were_updates:
            # If there were updates then continue the job
            # so it can monitor the progress of the update.
            continue_job_later(job_id, 'Stack update started')
        else:
            # If there were no updates then succeed the job immediately
            put_job_success(job_id, 'There were no stack updates')
    else:
        # If the stack doesn't already exist then create it instead of updating it
        create_stack(stack, template, service_params, client_cfn)
        # Continue the job so the pipeline will wait for the CFn stack to be created
        continue_job_later(job_id, 'Stack create started')


def check_stack_update_status(job_id, stack, client_cfn):
    """
    Monitors an already-running CloudFormation update/create.

    Succeeds, fails or continues the job depending on the stack status.

    Args:
        job_id: the CodePipeline job ID.
        stack: the stack to monitor.
        client_cfn: the boto3 client to use for interacting with CloudFormation.
    """
    status = get_stack_status(stack, client_cfn)
    if status in ['UPDATE_COMPLETE', 'CREATE_COMPLETE']:
        # If the update/create finished successfully then succeed the job and don't continue
        put_job_success(job_id, 'Stack update complete')

    elif status in ['UPDATE_IN_PROGRESS', 'UPDATE_ROLLBACK_IN_PROGRESS',
    'UPDATE_ROLLBACK_COMPLETE_CLEANUP_IN_PROGRESS', 'CREATE_IN_PROGRESS',
    'ROLLBACK_IN_PROGRESS', 'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS']:
        # If the job isn't finished yet then continue it
        continue_job_later(job_id, 'Stack update still in progress')
    else:
        # If the Stack is a state which isn't "in progress" or "complete"
        # then the stack update/create has failed so end the job with a failed result.
        put_job_failure(job_id, 'Update failed: ' + status)


def get_user_params(job_data):
    """
    Decodes the JSON user parameters and validates the required properties.

    Args:
        job_data: the job data structure containing the UserParameters string
                  which should be a valid JSON structure.

    Returns:
        The JSON parameters decoded as a dictionary.

    Raises:
        Exception: the JSON can't be decoded or a property is missing.
    """
    try:
        # Get the user parameters which contain the stack, artifact and file settings
        user_parameters = job_data['actionConfiguration']['configuration']['UserParameters']
        decoded_parameters = json.loads(user_parameters)
    except Exception as e:
        raise Exception('UserParameters could not be decoded as JSON')

    if 'ServiceStack' not in decoded_parameters:
        raise Exception(
            'Your UserParameters JSON must include the stack name of the service stack to update'
        )

    if 'TemplateArtifact' not in decoded_parameters:
        raise Exception(
            'Your UserParameters JSON must include the artifact name containing CFn templates'
        )

    if 'TemplateFile' not in decoded_parameters:
        raise Exception(
            'Your UserParameters JSON must include the template file name, ' \
            'i.e. the service template name'
        )

    if 'BuildArtifact' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the name of the build artifact')

    if 'BuildFile' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the name of the build file')

    if 'ClusterName' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the name of the ECS cluster')

    if 'ServiceRoleArn' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the service role ARN')

    if 'TargetGroupArn' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the target group ARN')

    if 'ECRRepoName' not in decoded_parameters:
        raise Exception('Your UserParameters JSON must include the name of the ECR repository')

    if 'ECRRepoAccountId' not in decoded_parameters:
        raise Exception(
            'Your UserParameters JSON must include the ID of the account hosting the ECR repository'
        )

    if 'DeploymentRegion' not in decoded_parameters:
        raise Exception(
            'Your UserParameters JSON must include the name of the region to deploy into'
        )

    return decoded_parameters


def setup_s3_client(job_data):
    """
    Creates an S3 client by using the credentials passed in the event by CodePipeline.
    These credentials can be used to access the artifact bucket.

    Args:
        job_data: the job data structure.

    Returns: an S3 client with the appropriate credentials.
    """
    key_id = job_data['artifactCredentials']['accessKeyId']
    key_secret = job_data['artifactCredentials']['secretAccessKey']
    session_token = job_data['artifactCredentials']['sessionToken']

    session = Session(
        aws_access_key_id=key_id,
        aws_secret_access_key=key_secret,
        aws_session_token=session_token
    )

    return session.client('s3', config=botocore.client.Config(signature_version='s3v4'))


def lambda_handler(event, context):
    """
    The Lambda function handler.

    If a continuing job then checks the CFn stack status and updates the job accordingly.

    If a new job then kick of an update or creation of the target CFn stack.

    Args:
        event: The event passed by Lambda
        context: The context passed by Lambda
    """
    try:
        job_id = event['CodePipeline.job']['id']  # extract the Job ID
        job_data = event['CodePipeline.job']['data']  # extract the Job Data
        params = get_user_params(job_data)  # extract the params
        artifacts = job_data['inputArtifacts']  # list of artifacts passed to the function

        stack = params['ServiceStack']

        artifact = params['TemplateArtifact']
        template_file = params['TemplateFile']

        buildartifact = params['BuildArtifact']
        build_file = params['BuildFile']

        cluster_name = params['ClusterName']
        service_role_arn = params['ServiceRoleArn']
        target_group_arn = params['TargetGroupArn']
        ecr_account = params['ECRRepoAccountId']
        ecr_reponame = params['ECRRepoName']
        deployment_region = params['DeploymentRegion']

        # Create a CloudFormation Client in the appropriate deployment region
        client_cfn = boto3.client('cloudformation', region_name=deployment_region)

        if 'continuationToken' in job_data:
            # If we're continuing then the create/update has already been triggered
            # we just need to check if it has finished.
            check_stack_update_status(job_id, stack, client_cfn)
        else:
            # Get the artifact details
            artifact_data = find_artifact(artifacts, artifact)
            build_artifact_data = find_artifact(artifacts, buildartifact)

            # Get S3 client to access artifact with
            s3 = setup_s3_client(job_data)

            # Get the JSON files out of the artifacts
            template = get_template(s3, artifact_data, template_file)
            buildfile = get_template(s3, build_artifact_data, build_file)

            # Grab the build tag from the build file.
            build_json = json.loads(buildfile)
            tag = build_json['tag']

            # Assign the specific parameters required by the CloudFormation template
            service_params = [
                {
                    'ParameterKey': 'ApplicationImageTag',
                    'ParameterValue': tag,
                    'UsePreviousValue': False
                },
                {
                    'ParameterKey': 'ClusterName',
                    'ParameterValue': cluster_name,
                    'UsePreviousValue': False
                },
                {
                    'ParameterKey': 'ECRRepoAccountId',
                    'ParameterValue': ecr_account,
                    'UsePreviousValue': False
                },
                {
                    'ParameterKey': 'ECRRepoName',
                    'ParameterValue': ecr_reponame,
                    'UsePreviousValue': False
                },
                {
                    'ParameterKey': 'ServiceRoleArn',
                    'ParameterValue': service_role_arn,
                    'UsePreviousValue': False
                },
                {
                    'ParameterKey': 'TargetGroupArn',
                    'ParameterValue': target_group_arn,
                    'UsePreviousValue': False
                }
            ]

            # Kick off a stack update or create
            start_update_or_create(job_id, stack, template, service_params, client_cfn)
    except Exception as e:
        # If any other exceptions which we didn't expect are raised
        # then fail the job and log the exception message.
        print('Function failed due to exception.')
        print(e)
        traceback.print_exc()
        put_job_failure(job_id, 'Function exception: ' + str(e))

    print('Function complete.')
    return "Complete."

from __future__ import print_function

import json
import tempfile
import traceback
import urllib
import zipfile

import boto3
from boto3.session import Session
import botocore


print('Loading function')

code_pipeline = boto3.client('codepipeline')


def find_artifact(artifacts, name):
    """
    Finds the artifact 'name' among the 'artifacts'.

    Args:
        artifacts: The list of artifacts available to the function
        name: The artifact we wish to use
    Returns:
        The artifact dictionary found
    Raises:
        Exception: If no matching artifact is found
    """
    for artifact in artifacts:
        if artifact['name'] == name:
            return artifact

    raise Exception('Input artifact named "{0}" not found in event'.format(name))


def put_job_success(job, message):
    """
    Notifies CodePipeline of a successful job.

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_success_result()
    """
    print('Putting job success')
    print(message)
    code_pipeline.put_job_success_result(jobId=job)


def put_job_failure(job, message):
    """
    Notifies CodePipeline of a failed job

    Args:
        job: The CodePipeline job ID
        message: A message to be logged relating to the job status

    Raises:
        Exception: Any exception thrown by .put_job_failure_result()
    """
    print('Putting job failure')
    print(message)
    code_pipeline.put_job_failure_result(
        jobId=job,
        failureDetails={'message': message, 'type': 'JobFailed'}
    )


def get_user_params(job_data):
    """
    Decodes the JSON user parameters and validates the required properties.

    Args:
        job_data: the job data structure containing the UserParameters string
                  which should be a valid JSON structure
    Returns:
        The JSON parameters decoded as a dictionary.
    Raises:
        Exception: the JSON can't be decoded or a property is missing.
    """
    try:
        # Get the user parameters which contain the stack, artifact and file settings
        user_parameters = job_data['actionConfiguration']['configuration']['UserParameters']
        decoded_parameters = json.loads(user_parameters)

    except Exception as e:
        # We're expecting the user parameters to be encoded as JSON
        # so we can pass multiple values. If the JSON can't be decoded
        # then fail the job with a helpful message.
        raise Exception('UserParameters could not be decoded as JSON')

    if 'TemplateArtifact' not in decoded_parameters:
        # Validate that the artifact name is provided, otherwise fail the job
        # with a helpful message.
        raise Exception(
            'Your UserParameters JSON must include the artifact name ' \
            'containing cloudformation templates'
        )

    if 'TemplateFile' not in decoded_parameters:
        # Validate that the template file is provided,
        # otherwise fail the job with a helpful message.
        raise Exception(
            'Your UserParameters JSON must include the template file name, ' \
            'i.e. the service template name'
        )

    if 'BuildArtifact' not in decoded_parameters:
        # Validate that the build artifact is provided,
        # otherwise fail the job with a helpful message.
        raise Exception('Your UserParameters JSON must include the name of the build artifact')

    if 'BuildFile' not in decoded_parameters:
        # Validate that the build filename is provided,
        # otherwise fail the job with a helpful message.
        raise Exception('Your UserParameters JSON must include the name of the build file')

    if 'DeploymentBucketName' not in decoded_parameters:
        # Validate that the deployment bucket name is provided,
        # otherwise fail the job with a helpful message.
        raise Exception(
            'Your UserParameters JSON must include the name of the deployment ' \
            'bucket in the production account'
        )

    if 'DeploymentRegion' not in decoded_parameters:
        # Validate that the deployment region is provided,
        # otherwise fail the job with a helpful message.
        raise Exception(
            'Your UserParameters JSON must include the name of the region to deploy into'
        )

    return decoded_parameters


def transfer_artifact(s3_local, s3_deploy, artifact, target_bucket, target_key):
    """
    Downloads the artifact from the S3 artifact store to a temporary file
    then copies the temporary file to the deployment bucket in the prod account.

    Args:
        s3_local: Local S3 client
        s3_deployment: Deployment S3 client
        artifact: The artifact file to transfer
        target_bucket: Name of bucket in production account.
        target_key: Name of key to use when placing the artifact in the target bucket
    Returns:
        Indication of success
    Raises:
        Exception: Any exception thrown while downloading the artifact or copying it
    """
    tmp_file = tempfile.NamedTemporaryFile()
    bucket = artifact['location']['s3Location']['bucketName']
    key = artifact['location']['s3Location']['objectKey']

    with tempfile.NamedTemporaryFile() as tmp_file:
        s3_local.download_file(bucket, key, tmp_file.name)
        s3_deploy.upload_file(
            tmp_file.name,
            target_bucket,
            target_key,
            ExtraArgs={'ACL': 'bucket-owner-read'}
        )


def setup_local_s3_client(job_data):
    """
    Creates an S3 client by using the credentials passed in the event by CodePipeline.
    These credentials can be used to access the artifact bucket.

    Args:
        job_data: The job data structure
        region_name: The name of the region for this client.
    Returns:
        An S3 client with the appropriate credentials
    """
    key_id = job_data['artifactCredentials']['accessKeyId']
    key_secret = job_data['artifactCredentials']['secretAccessKey']
    session_token = job_data['artifactCredentials']['sessionToken']

    session = Session(
        aws_access_key_id=key_id,
        aws_secret_access_key=key_secret,
        aws_session_token=session_token
    )

    return session.client('s3', config=botocore.client.Config(signature_version='s3v4'))

def lambda_handler(event, context):
    """
    The Lambda function handler.

    If a continuing job then checks the CloudFormation stack status and updates the job accordingly.

    If a new job then kick of an update or creation of the target CloudFormation stack.

    Args:
        event: The event passed by Lambda
        context: The context passed by Lambda
    """
    try:
        job_id = event['CodePipeline.job']['id']  # extract the Job ID
        job_data = event['CodePipeline.job']['data']  # extract the Job Data
        params = get_user_params(job_data)  # extract the params
        artifacts = job_data['inputArtifacts']  # list of artifacts passed to the function

        artifact = params['TemplateArtifact']
        template_file = params['TemplateFile']

        buildartifact = params['BuildArtifact']
        build_file = params['BuildFile']

        deployment_bucket = params['DeploymentBucketName']
        deployment_region = params['DeploymentRegion']

        if 'continuationToken' in job_data:
            print('Waiting for transfer to production...')
        else:
            artifact_data = find_artifact(artifacts, artifact)
            build_artifact_data = find_artifact(artifacts, buildartifact)

            # Get an S3 client to access build artifacts in this pipeline.
            s3_local = setup_local_s3_client(job_data)

             # Grab a second s3 client to perform the deployment transfer
            s3_deploy = boto3.client('s3', region_name=deployment_region)

            transfer_artifact(
                s3_local,
                s3_deploy,
                artifact_data,
                deployment_bucket,
                'artifacts/cloudformation/templates.zip'
            )
            transfer_artifact(
                s3_local,
                s3_deploy,
                build_artifact_data,
                deployment_bucket,
                'artifacts/build/build.zip'
            )

            print('Transfer complete!')

            put_job_success(job_id, 'Successfully transferred files to ' + deployment_bucket)
    except Exception as e:
        # If any other exceptions which we didn't expect are raised
        # then fail the job and log the exception message.
        print('Function failed due to exception.')
        print(e)
        traceback.print_exc()
        put_job_failure(job_id, 'Function exception: ' + str(e))

    print('Function complete.')
    return "Complete."
# CICD Demo

This CICD demo demonstrates how to build, test, deploy a WAR file to a Docker container running on EC2 using AWS services such as EC2, EC2 Container Service, ECR, CodeCommit, CodeBuild, CodePipeline, CloudFormation, Lambda, S3 and SNS.

![HighLevelArc](./images/Introduction-HighLevelArc.png)

![Pipeline](./images/Introduction-PipelineDetails.png)


## Notes

This CICD demo requires 3 AZs so it might run in the following regions:

* ap-southeast-2 (Sydney)
* us-east-1 (N. Virginia)
* us-east-2 (Ohio)
* us-west-1 (N. California)
* us-west-2 (Oregon)

Support for other regions with 3 AZs such as ap-northeast-1 (Tokyo), eu-central-1 (Frankfurt) eu-west-1 (Dublin), sa-east-1 (Sao Paulo) will be added shortly.


## Requirements

- python 2.7+ (python3 has not been tested)
- Install pip using this [page](https://pip.pypa.io/en/stable/installing/).
- Install requirements with `pip2 install -r requirements.txt`
- For Mac OS users, ensure you do not have any CodeCommit keys in your KeyChain from previous demo which might be expired.

Note: `python` and `pip` commands might work as well.


## Prepare Demo Phase

This demo requires some S3 buckets and to setup minimal infrastructure such as VPC and networking (route tables, subnets, NAT Gateway), groups and users, security groups, ECS and Application Load Balancer. It takes approximately 10-15 minutes to create them so it is a good idea to prepare the demo in advance...

### Execution of Prepare Demo Phase

1. Clone the file `deploy-pipeline-config.yml` and give it a custom name such as `deploy-pipeline-config-<demoYYYY-MM-DD>.yml`
2. In the file `deploy-pipeline-config-<demoYYYY-MM-DD>.yml`, replace the place holder `<UNIQUE_PREFIX>` with an unique prefix everywhere in the file in order to have unique names for AWS resources (S3 buckets in particular).
3. Modify the key _EmailSubscriber_ key to set an appropriate value (your API Talent email address for example).
4. Then run `python2 deploy-pipeline.py -c <Your Config File from Step 2> -a prepare-demo`

Two CloudFormation stacks are creating so have a look on the CloudFormation service page or keep an eye on your terminal to check when they are ready.

### Outputs of Prepare Demo Phase

Pre requisites buckets are available as outputs of the CloudFormation _\*demo-pipeline-prereq-buckets_ stack.

![OutputsBuckets](./images/PrepareDemo-OutputsBuckets.png)

Note that the Maven site will be published to _MavenSiteBucketName_ bucket after the stage _DemoAppStageSourceBuildProj_ of the pipeline. In this example, the Maven site will be accessible at http://pm-demo-pipeline-maven-site.s3-website-ap-southeast-2.amazonaws.com/. More details about website hosting with S3 [here](http://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html).

The future URL of the application is available as output of the nested CloudFormation _\*demo-pipeline-env-ECSCluster\*_ stack.

![OutputsLoadBalancerDNS](./images/PrepareDemo-OutputsLoadBalancerDNS.png)

It obviously returns a `502 Bad Gateway` error at this stage as the application has not been deployed yet.

![LoadBalancerDNS_502](./images/PrepareDemo-LoadBalancerDNS_502.png)

You are now all prepared to run the CICD Demo!


## Run Demo Phase

### Execution of Run Demo Phase
To run the demo, simply execute `python2 deploy-pipeline.py -c <CustomConfigFile.yml> -a run-demo`.

#### High Level Overview

This command will create the CICD pipeline, checkout the CodeCommit repositories locally, copy the code to these checkouts and then push an initial commit on both of them. Then the pipeline will be kicked off to build and deploy your app.

Note: because we are using two CodeCommit repositories as Source providers, each initial commit is built. So you will see two executions of the pipeline running at the same time (race condition). You can safely ignore the second build. 

#### Detailed Deep Dive

##### SNS Topic
During the creation of the pipeline by CloudFormation, a SNS topic is created and will send a notification subscription request to your configured _EmailSubscriber_. This SNS topic will automatically send emails indicating that your approval is required to deploy to production:

![NotificationSubscription](./images/RunDemo-00a-NotificationSubscription.png)

![NotificationSubscriptionConfirmed](./images/RunDemo-00b-NotificationSubscriptionConfirmed.png)


##### CodePipeline

Once the pipeline created by CloudFormation, you can retrieve its access URL from the ouputs of CloudFormation pipeline stack or you can access the AWS CodePipeline service directly:

![PipelineURL](./images/RunDemo-00c-PipelineURL.png)

![PipelineMainPage](./images/RunDemo-00d-PipelineMainPage.png)

Note: the first 30 days of a pipeline are free so `No Charge` indicates which pipelines are free or not. 

##### CodeCommit
The CodeCommit repositories are also been created by CloudFormation and they are available on the CodeCommit service page. Initial commits have been made against both of them:

![CodeCommitRepos](./images/RunDemo-01a-CodeCommitRepos.png)

![CodeCommitRepos](./images/RunDemo-01b-CodeCommitInitialCommit.png)


##### Execution of the Pipeline

The CodeCommit repositories act as Source Providers so their initial commit automatically trigger an execution of the pipeline:

![CodeCommitRepos](./images/RunDemo-01c-SourceProviders.png)

Once this Source stage complete, the pipeline progresses to the Build stage building the Demo App using AWS CodeBuild:

![CodeBuildDemoApp](./images/RunDemo-02a-CodeBuildDemoApp.png)

Logs of each build via AWS CodeBuild service page. They are also streamed in real time to CloudWatch Logs (AWS CodeBuild creates the log group for your):

![CodeBuildPhaseLogs](./images/RunDemo-02b-CodeBuildPhaseLogs.png)

![CodeBuildCloudWatchLogs](./images/RunDemo-02c-CodeBuildCloudWatchLogs.png)

If build successful, its phases are all in _Succeeded_ state:

![CodeBuildPhasesComplete](./images/RunDemo-02d-CodeBuildPhasesComplete.png)

Note: the Maven site has been published to S3 and it is now available. In this example, the Maven site is accessible at http://pm-demo-pipeline-maven-site.s3-website-ap-southeast-2.amazonaws.com/

![CodeBuildDockerCFnPackager](./images/RunDemo-02e-DemoAppMavenSite.png)

The pipeline keeps progressing to the next Build phase to build the Docker image and to package the CloudFormation templates:

![CodeBuildDockerCFnPackager](./images/RunDemo-03a-CodeBuildDockerCFnPackager.png)

After that a Lambda is invoked to deployed the Docker container via a CloudFormation stack named _\*demo-app-service_

![LambdaDeployToDemo](./images/RunDemo-04a-LambdaDeployToDemo.png)

![DemoCloudFormationStack](./images/RunDemo-04b-DemoCloudFormationStack.png)

Once deployed, the app is available via the LoadBalancer URL which was previously returning an `502 Bad Gateway` error. The URL now displays the expected result:

![DemoCloudFormationStack](./images/RunDemo-04c-DemoAppAvailable.png)

You automatically receive a notification at email address you have configured indicating that a manual approval is required to deploy this first build to production:

![PushToProdNotificationEmail](./images/RunDemo-05a-PushToProdNotificationEmail.png)

![PipelineManualApproval](./images/RunDemo-05b-PipelineManualApproval.png)

If you decide to approve it, the execution of the pipeline is resumed and completes the last stage to deploy your build to production:

![DeployToProd](./images/RunDemo-05c-DeployToProd.png)

![DeployToProdComplete](./images/RunDemo-05c-DeployToProdComplete.png)

That's it! You reach the end of this demo. Congratulations!

You can now modify the code of the app or the Docker container (via local checkouts available in your working directory), publish your changes to CodeCommit and let CodePipeline build and deploy them to the Demo environment. You keep the ability to promote a build to production by manually approving or rejecting a build.

### Examples of Failures

#### Failed Build

In case of a failed build, the pipeline will be stopped and you might optionally retry it if you want (after inspection of the logs for example):

![FailedBuild](./images/Examples-FailedBuild.png)


#### Rejected Approval

In you decide to reject a PushToProd, the pipeline does not progress further (`Push-To-Prod` is not invoked in that case) and your production is not impacted:

![FailedBuild](./images/Examples-RejectedApproval.png)


## Clean Up Demo Phase

It is time to clean up after yourself!

Because everything has been created via CloudFormation (thanks Infrastructure-As-Code), it is easy to delete all the AWS resources spinned up by the different stacks during this demo.

To clean them up, simply run `python2 deploy-pipeline.py -c <CustomConfigFile.yml> -a cleanup-demo`.

Note: the code will also clean up "manually" the CloudWatch Logs created by AWS Lambda and AWS CodeBuild. Also the pre requisites S3 buckets are emptied to avoid any error on deletion. The local checkouts of the CodeCommit repositories are deleted (folder prefixed by `__demo-cicd`).

Finally delete manually your `<CustomConfigFile.yml>` config file.
